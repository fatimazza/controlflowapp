package id.co.iconpln.controlflowapp.model.myProfile

data class ProfileLoginUser(
    val email: String,
    val password: String
)
