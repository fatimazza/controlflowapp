package id.co.iconpln.controlflowapp.model.myUser

data class SingleUserResponse<T>(
    val data : UserDataResponse
)
